import 'busca_endereco_controller.dart';
import 'package:dio/dio.dart';
import 'repositories/busca_endereco_repository.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'busca_endereco_page.dart';

class BuscaEnderecoModule extends ChildModule {
  @override
  List<Bind> get binds => [
    Bind((i) => BuscaEnderecoController(i.get())),
    Bind((i) => BuscaEnderecoRepository()),



      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute,
            child: (_, args) => BuscaEnderecoPage()),
      ];

  static Inject get to => Inject<BuscaEnderecoModule>.of();
}
