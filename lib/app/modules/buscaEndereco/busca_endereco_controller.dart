import 'package:flutter/material.dart';
import 'package:flutter_app/app/modules/buscaEndereco/repositories/busca_endereco_repository.dart';
import 'package:flutter_app/share/models/cep_model.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'busca_endereco_controller.g.dart';

@Injectable()
class BuscaEnderecoController = _BuscaEnderecoController
    with _$BuscaEnderecoController;



abstract class _BuscaEnderecoController with Store {
  BuscaEnderecoRepository _repository;
  @observable
  String cep = "";



  _BuscaEnderecoController(this._repository);



  @observable
  CepModel cepModel;

  @action
  void setCep(String value) => cep = value;

  @action
  Future<CepModel> getCep(String cep) async {
    try {
      cepModel = await _repository.getCep(cep);
    } catch (e) {
      throw Exception(e);
    }
    return cepModel;
  }


}
