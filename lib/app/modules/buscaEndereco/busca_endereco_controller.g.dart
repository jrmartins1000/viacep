// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'busca_endereco_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BuscaEnderecoController on _BuscaEnderecoController, Store {
  final _$cepAtom = Atom(name: '_BuscaEnderecoController.cep');

  @override
  String get cep {
    _$cepAtom.reportRead();
    return super.cep;
  }

  @override
  set cep(String value) {
    _$cepAtom.reportWrite(value, super.cep, () {
      super.cep = value;
    });
  }

  final _$cepModelAtom = Atom(name: '_BuscaEnderecoController.cepModel');

  @override
  CepModel get cepModel {
    _$cepModelAtom.reportRead();
    return super.cepModel;
  }

  @override
  set cepModel(CepModel value) {
    _$cepModelAtom.reportWrite(value, super.cepModel, () {
      super.cepModel = value;
    });
  }

  final _$getCepAsyncAction = AsyncAction('_BuscaEnderecoController.getCep');

  @override
  Future<CepModel> getCep(String cep) {
    return _$getCepAsyncAction.run(() => super.getCep(cep));
  }

  final _$_BuscaEnderecoControllerActionController =
      ActionController(name: '_BuscaEnderecoController');

  @override
  void setCep(String value) {
    final _$actionInfo = _$_BuscaEnderecoControllerActionController.startAction(
        name: '_BuscaEnderecoController.setCep');
    try {
      return super.setCep(value);
    } finally {
      _$_BuscaEnderecoControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
cep: ${cep},
cepModel: ${cepModel}
    ''';
  }
}
