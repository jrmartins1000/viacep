import 'package:dio/dio.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_app/app/initial/app_widget.dart';
import 'package:flutter_app/app/modules/buscaEndereco/busca_endereco_module.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AppModule extends MainModule {
  @override
  // TODO: implement binds
  List<Bind> get binds => [
        Bind((i) => Dio(BaseOptions(baseUrl: 'https://viacep.com.br/ws/08280680/json/', connectTimeout: 5000))),
      ];

  @override
  // TODO: implement routers
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, module: BuscaEnderecoModule()),
      ];

  @override
  // TODO: implement bootstrap
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
