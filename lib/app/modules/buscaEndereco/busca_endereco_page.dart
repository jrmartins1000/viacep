import 'package:flutter/material.dart';
import 'package:flutter_app/share/models/cep_model.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'busca_endereco_controller.dart';

class BuscaEnderecoPage extends StatefulWidget {
  @override
  _BuscaEnderecoPageState createState() => _BuscaEnderecoPageState();
}

class _BuscaEnderecoPageState
    extends ModularState<BuscaEnderecoPage, BuscaEnderecoController> {
  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Busca Endereço', style: TextStyle(
          color: Colors.white
        ),),
      ),
      body: Container(
        padding: EdgeInsets.only(
          top: 20,
          left: 20,
          right: 20,
          bottom: 40,
        ),
        child: Column(
          children: [
            _itensColum(),
            _dataCep(),
          ],
        ),
      ),
    );
  }

  Widget _itensColum() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Observer(
          builder: (context) {
            return Container(
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(32),
              ),
              padding: EdgeInsets.only(left: 16),
              child: TextField(
                onChanged: controller.setCep,
                controller: _controller,
                keyboardType: TextInputType.number,
                autofocus: true,
                decoration: InputDecoration(
                    labelText: "Digite o cep: ex 05428200",
                    border: InputBorder.none),
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            );
          },
        ),
        SizedBox(height: 15),

        //Botão buscar cep
        _buttonBuscar(),
        SizedBox(height: 15),
      ],
    );
  }

  //Botão de Busca Endereco
  Widget _buttonBuscar() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: Colors.orangeAccent,
      ),
      child: Text(
        'Buscar Cep',
        style: TextStyle(
          fontSize: 20,
        ),
      ),
      onPressed: () {
        setState(() {});
        controller.getCep(controller.cep).then((value) {
          controller.cepModel = value;
          _controller.clear();
        }).catchError((error) {});
      },
    );
  }

  //Dados Endereco
  Widget _dataCep() {
    return Observer(builder: (context) {
      return Column(
        children: [

          controller.cepModel != null
              ? Text(
            controller.cepModel.logradouro,
            style: TextStyle(
              color: Colors.black,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          )
              : Text(''),

          SizedBox(height: 15),

          controller.cepModel != null
              ? Text(' Bairro: ${controller.cepModel.bairro} ',

            style: TextStyle(
              color: Colors.black,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          )
              : Text(''),

          SizedBox(height: 15),


          controller.cepModel != null
              ? Text(
            ' Cidade: ${controller.cepModel.localidade} ',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                )
              : Text(''),
        ],
      );
    });
  }
}
