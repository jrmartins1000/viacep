import 'package:flutter_app/share/models/cep_model.dart';

abstract class ICep {
  Future<CepModel> getCep(String cep);
}
