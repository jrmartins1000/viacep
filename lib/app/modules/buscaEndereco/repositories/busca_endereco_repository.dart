import 'dart:convert';

import 'package:flutter_app/app/modules/buscaEndereco/repositories/cep_interface.dart';
import 'package:flutter_app/share/models/cep_model.dart';
import 'package:flutter_app/share/utilities/constants.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:http/http.dart' as http;

@Injectable()
class BuscaEnderecoRepository extends ICep {
  @override
  Future<CepModel> getCep(String cep) async {
    CepModel model;

    try {
      final response = await http.get('${Constants.BASE_URL}${cep}${Constants.TIPO_URL}');
      // final response = await http.get('https://viacep.com.br/ws/${cep}/json/');
      if (response.statusCode == 200) {
        model = CepModel.fromJson(jsonDecode(response.body));
      } else {
        print('Erro ao carregar serviço' + response.statusCode.toString());
        return null;
      }

      // TODO: implement getCep

    } catch (error, stacktrace) {
      print("Erro ao carregar " + stacktrace.toString());
      return null;
    }

    return model;
  }
}
